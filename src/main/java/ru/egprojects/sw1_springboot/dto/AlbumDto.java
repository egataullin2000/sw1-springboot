package ru.egprojects.sw1_springboot.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.egprojects.sw1_springboot.model.Album;
import ru.egprojects.sw1_springboot.model.FileInfo;

import java.util.List;

@Data
@AllArgsConstructor
public class AlbumDto {
    private Long id;
    private String title;
    private String poster;
    private String background;
    private String description;
    private String genre;
    private int year;
    private List<FileInfo> audios;

    public static AlbumDto from(Album album) {
        FileInfo poster = album.getPoster();
        FileInfo background = album.getBackground();
        List<FileInfo> audios = album.getAudios();

        return new AlbumDto(
                album.getId(),
                album.getTitle(),
                poster == null ? null : "/files/" + poster.getStorageFileName(),
                background == null ? null : "/files/" + background.getStorageFileName(),
                album.getDescription(),
                album.getGenre(),
                album.getYear(),
                audios
        );
    }
}
