package ru.egprojects.sw1_springboot.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.egprojects.sw1_springboot.model.SupportMessage;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class SupportMessageDto {
    private String sender;
    private String text;
    private String receiver;

    public static SupportMessageDto from(SupportMessage message) {
        if (message == null) return null;

        return SupportMessageDto.builder()
                .sender(message.getSender().getUsername())
                .text(message.getText())
                .receiver(message.getReceiver().getUsername())
                .build();
    }

    public static List<SupportMessageDto> from(List<SupportMessage> messages) {
        return messages.stream()
                .map(SupportMessageDto::from)
                .collect(Collectors.toList());
    }
}
