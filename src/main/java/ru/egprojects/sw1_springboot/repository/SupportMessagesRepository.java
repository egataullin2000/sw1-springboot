package ru.egprojects.sw1_springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.egprojects.sw1_springboot.model.SupportMessage;
import ru.egprojects.sw1_springboot.model.User;

import java.util.List;

public interface SupportMessagesRepository extends JpaRepository<SupportMessage, Long> {
    List<SupportMessage> getAllBySenderEmail(String login);

    List<SupportMessage> getAllByReceiverEmail(String login);

    List<SupportMessage> getAllBySenderEmailAndReceiverEmail(String senderLogin, String receiverLogin);

    @Query("from SupportMessage message where message.sender <> :admin")
    List<SupportMessage> getAllMessages(@Param("admin") User admin);
}
