package ru.egprojects.sw1_springboot.model;

import io.swagger.annotations.ApiModel;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@ToString(exclude = "artist")
@ApiModel(description = "All details about the Album")
public class Album {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "artist_id")
    private User artist;
    private String title;
    @OneToOne
    private FileInfo poster;
    @OneToOne
    private FileInfo background;
    private String description;
    private String genre;
    private int year;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FileInfo> audios;
}
