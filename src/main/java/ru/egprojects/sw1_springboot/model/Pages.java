package ru.egprojects.sw1_springboot.model;

public class Pages {
    public static final String HOME = "/";
    public static final String SIGN_UP = "/signUp";
    public static final String SIGN_IN = "/signIn";
    public static final String USERS = "/users";
    public static final String PROFILE = "/profile";
    public static final String EDIT_PROFILE = PROFILE + "/edit";
    public static final String ALBUM = "/album";
    public static final String CREATE_ALBUM = PROFILE + ALBUM + "/create";
    public static final String EDIT_ALBUM = ALBUM + "/edit";
    public static final String SEARCH = "/search";
    private static final String CONFIRMATION = "/confirm";
    public static final String EMAIL_CONFIRMATION = CONFIRMATION + "/email";
    public static final String PHONE_CONFIRMATION = CONFIRMATION + "/phone";
    private static final String API = "/api";
    public static final String AUTH_API = API + "/auth";
    public static final String ALBUMS_API = API + "/albums";
    public static final String USERS_API = API + "/users";
}
