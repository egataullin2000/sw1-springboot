package ru.egprojects.sw1_springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.egprojects.sw1_springboot.dto.AlbumDto;
import ru.egprojects.sw1_springboot.dto.UserDto;
import ru.egprojects.sw1_springboot.model.Album;
import ru.egprojects.sw1_springboot.model.Pages;
import ru.egprojects.sw1_springboot.model.User;
import ru.egprojects.sw1_springboot.repository.AlbumRepository;
import ru.egprojects.sw1_springboot.security.details.UserDetailsImpl;
import ru.egprojects.sw1_springboot.service.FileStorageService;

import java.util.Arrays;
import java.util.stream.Collectors;

@Controller
@RequestMapping(Pages.EDIT_ALBUM)
public class EditAlbumController {

    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping
    public String get(Authentication authentication, @RequestParam Long id, Model model) {
        User user = ((UserDetailsImpl) authentication.getPrincipal()).getUser();
        Album album = albumRepository.getOne(id);
        if (!album.getArtist().getId().equals(user.getId())) {
            return "redirect:" + Pages.ALBUM + "?id=" + id;
        }
        model.addAttribute("authenticated", true);
        model.addAttribute("user", UserDto.from(user));
        model.addAttribute("album", AlbumDto.from(albumRepository.getOne(id)));

        return "edit_album";
    }

    @PostMapping
    public String post(
            @RequestParam Long id,
            @RequestParam String title,
            @RequestParam MultipartFile poster,
            @RequestParam MultipartFile background,
            @RequestParam String genre,
            @RequestParam int year,
            @RequestParam MultipartFile[] audios
    ) {
        Album album = albumRepository.getOne(id);
        album.setTitle(title);
        album.setPoster(fileStorageService.saveFile(poster));
        album.setBackground(fileStorageService.saveFile(background));
        album.setGenre(genre);
        album.setYear(year);
        album.getAudios().addAll(
                Arrays.stream(audios)
                        .map(multipartFile -> fileStorageService.saveFile(multipartFile))
                        .collect(Collectors.toList())
        );

        albumRepository.save(album);

        return "redirect:" + Pages.ALBUM + "?id=" + id;
    }

}
