package ru.egprojects.sw1_springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.egprojects.sw1_springboot.dto.AlbumDto;
import ru.egprojects.sw1_springboot.dto.UserDto;
import ru.egprojects.sw1_springboot.model.Album;
import ru.egprojects.sw1_springboot.model.Pages;
import ru.egprojects.sw1_springboot.repository.AlbumRepository;

@Controller
@RequestMapping(Pages.ALBUM)
public class AlbumController {
    @Autowired
    private AlbumRepository albumRepository;

    @GetMapping
    public String get(Authentication authentication, @RequestParam Long id, Model model) {
        model.addAttribute("authenticated", authentication != null);
        Album album = albumRepository.getOne(id);
        model.addAttribute("album", AlbumDto.from(album));
        model.addAttribute("artist", UserDto.from(album.getArtist()));

        return "album";
    }
}
