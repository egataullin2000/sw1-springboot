package ru.egprojects.sw1_springboot.controller.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.egprojects.sw1_springboot.dto.UserDto;
import ru.egprojects.sw1_springboot.model.Pages;
import ru.egprojects.sw1_springboot.service.UserService;

import java.util.List;

@RestController
@RequestMapping(Pages.USERS_API)
@Api(value = "User Management System")
public class UsersApiController {

    @Autowired
    private UserService userService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Get user by id", response = UserDto.class)
    public UserDto getUserById(@PathVariable Long id) {
        return userService.getById(id);
    }

    @GetMapping
    @ApiOperation(value = "Get a list of users", response = List.class)
    public List<UserDto> getUsers() {
        return userService.getAll();
    }

    @PostMapping("/confirm")
    @ApiOperation(value = "Confirm user by confirmation code", response = UserDto.class)
    public UserDto confirm(@RequestParam String code) {
        return userService.confirmUser(code);
    }

    @GetMapping("/artists")
    @ApiOperation(value = "Get a list of available artists", response = List.class)
    public List<UserDto> getArtists(@RequestParam int count) {
        return userService.getLastArtists(count);
    }

}
