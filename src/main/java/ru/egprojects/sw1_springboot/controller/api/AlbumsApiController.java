package ru.egprojects.sw1_springboot.controller.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.egprojects.sw1_springboot.dto.AlbumDto;
import ru.egprojects.sw1_springboot.model.Album;
import ru.egprojects.sw1_springboot.model.Pages;
import ru.egprojects.sw1_springboot.model.User;
import ru.egprojects.sw1_springboot.security.details.UserDetailsImpl;
import ru.egprojects.sw1_springboot.service.AlbumService;

import java.util.List;


@RestController
@RequestMapping(Pages.ALBUMS_API)
@Api(value = "Album Management System")
public class AlbumsApiController {

    @Autowired
    private AlbumService albumService;

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/create")
    @ApiOperation(value = "Create an album", response = AlbumDto.class)
    public AlbumDto createAlbum(
            @RequestParam String title,
            @RequestParam String genre,
            @RequestParam int year,
            @RequestParam String description,
            @RequestParam MultipartFile poster,
            @RequestParam MultipartFile background,
            @RequestParam MultipartFile[] audios
    ) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = ((UserDetailsImpl) authentication.getDetails()).getUser();
        Album album = albumService.createAlbum(user, title, genre, year, description, poster, background, audios);

        return AlbumDto.from(album);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get album by id", response = AlbumDto.class)
    public AlbumDto getAlbumById(@PathVariable Long id) {
        return albumService.getAlbumById(id);
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/update")
    @ApiOperation(value = "Update album", response = AlbumDto.class)
    public AlbumDto updateAlbum(@RequestBody Album album) {
        albumService.updateAlbum(album);
        return AlbumDto.from(album);
    }

    @GetMapping("/search")
    @ApiOperation(value = "Search album by query", response = List.class)
    public List<AlbumDto> searchAlbum(@RequestParam String query, @RequestParam(required = false) Integer year) {
        return albumService.search(query, year);
    }

    @GetMapping("/last")
    @ApiOperation(value = "Get last albums", response = List.class)
    public List<AlbumDto> getLastAlbums(@RequestParam int count) {
        return albumService.getLast(count);
    }

}
