package ru.egprojects.sw1_springboot.controller.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.egprojects.sw1_springboot.dto.SignInDto;
import ru.egprojects.sw1_springboot.dto.SignUpDto;
import ru.egprojects.sw1_springboot.dto.TokenDto;
import ru.egprojects.sw1_springboot.model.Pages;
import ru.egprojects.sw1_springboot.service.SignUpService;
import ru.egprojects.sw1_springboot.service.TokenService;

@RestController
@RequestMapping(Pages.AUTH_API)
@Api(value = "Authentication Management System")
public class AuthApiController {

    @Autowired
    private SignUpService signUpService;

    @Autowired
    private TokenService tokenService;

    @PostMapping("/registration")
    @ApiOperation(value = "Sign up", response = ResponseEntity.class)
    public ResponseEntity<?> signUp(@RequestBody SignUpDto signUpDto) {
        signUpService.signUp(signUpDto);
        return ResponseEntity.accepted().build();
    }

    @PostMapping("/login")
    @ApiOperation(value = "Sign in", response = TokenDto.class)
    public TokenDto signIn(@RequestBody SignInDto signInDto) {
        return tokenService.signIn(signInDto);
    }

}
