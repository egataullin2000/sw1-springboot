package ru.egprojects.sw1_springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.egprojects.sw1_springboot.model.AuthData;
import ru.egprojects.sw1_springboot.model.User;
import ru.egprojects.sw1_springboot.security.details.UserDetailsImpl;
import ru.egprojects.sw1_springboot.service.SupportMessageService;
import ru.egprojects.sw1_springboot.service.UserService;

// Поддержка как бы есть, но после подключения JWT у меня поломалась авторизация :(
// поэтому не могу показать в браузере

@Controller
public class SupportController {

    @Autowired
    private UserService userService;

    @Autowired
    private SupportMessageService messageService;

    @GetMapping("/support")
    public String getPage(Authentication authentication, Model model) {
        AuthData authData = ((UserDetailsImpl) authentication.getPrincipal()).getAuthData();
        User user = ((UserDetailsImpl) authentication.getPrincipal()).getUser();
        User admin = userService.getAdmin();
        model.addAttribute("user", user);
        if (authData.getRole().equals(AuthData.Role.ADMIN)) {
            model.addAttribute("users", userService.getAll());
            model.addAttribute("allmessages", messageService.getAllMessages(admin));
        }
        model.addAttribute("admin", admin.getEmail());
        model.addAttribute("messages", messageService.getDialog(user.getEmail(), admin.getEmail()));

        return "support";
    }
}
