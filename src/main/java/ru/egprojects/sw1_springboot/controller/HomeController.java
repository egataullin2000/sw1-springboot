package ru.egprojects.sw1_springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.egprojects.sw1_springboot.dto.AlbumDto;
import ru.egprojects.sw1_springboot.dto.UserDto;
import ru.egprojects.sw1_springboot.model.Pages;
import ru.egprojects.sw1_springboot.service.AlbumService;
import ru.egprojects.sw1_springboot.service.UserService;

import java.util.List;

@Controller
@RequestMapping(Pages.HOME)
public class HomeController {

    @Autowired
    private UserService userService;

    @Autowired
    private AlbumService albumService;

    @GetMapping
    public String get(Authentication authentication, Model model) {
        model.addAttribute("authenticated", authentication != null);
        List<UserDto> lastArtists = userService.getLastArtists(10);
        model.addAttribute("artists", lastArtists);
        List<AlbumDto> lastAlbums = albumService.getLast(10);
        model.addAttribute("albums", lastAlbums);

        return "home";
    }

}
