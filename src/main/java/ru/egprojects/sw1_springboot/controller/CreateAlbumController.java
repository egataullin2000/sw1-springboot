package ru.egprojects.sw1_springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.egprojects.sw1_springboot.dto.UserDto;
import ru.egprojects.sw1_springboot.model.Album;
import ru.egprojects.sw1_springboot.model.Pages;
import ru.egprojects.sw1_springboot.model.User;
import ru.egprojects.sw1_springboot.security.details.UserDetailsImpl;
import ru.egprojects.sw1_springboot.service.AlbumService;


@Controller
@RequestMapping(Pages.CREATE_ALBUM)
public class CreateAlbumController {

    @Autowired
    private AlbumService albumService;

    @PostMapping
    public String post(
            Authentication authentication,
            @RequestParam String title,
            @RequestParam String genre,
            @RequestParam int year,
            @RequestParam String description,
            @RequestParam MultipartFile poster,
            @RequestParam MultipartFile background,
            @RequestParam MultipartFile[] audios
    ) {
        User user = ((UserDetailsImpl) authentication.getPrincipal()).getUser();
        Album album = albumService.createAlbum(user, title, genre, year, description, poster, background, audios);

        return "redirect:" + Pages.ALBUM + "?id=" + album.getId();
    }

    @GetMapping
    public String get(Authentication authentication, Model model) {
        model.addAttribute("authenticated", true);
        model.addAttribute("user", UserDto.from(
                ((UserDetailsImpl) authentication.getPrincipal()).getUser()
        ));

        return "create_album";
    }

}
