package ru.egprojects.sw1_springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.egprojects.sw1_springboot.dto.UserDto;
import ru.egprojects.sw1_springboot.model.AuthData;
import ru.egprojects.sw1_springboot.model.User;
import ru.egprojects.sw1_springboot.repository.AuthDataRepository;
import ru.egprojects.sw1_springboot.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthDataRepository authDataRepository;

    @Override
    public UserDto getById(Long id) {
        return UserDto.from(userRepository.getOne(id));
    }

    @Override
    public List<UserDto> getAll() {
        return userRepository.findAll()
                .stream()
                .map(UserDto::from)
                .collect(Collectors.toList());
    }

    @Override
    public UserDto confirmUser(String confirmationCode) {
        Optional<AuthData> authDataOptional = authDataRepository.findByConfirmationCode(confirmationCode);
        if (authDataOptional.isPresent()) {
            User user = authDataOptional.get().getUser();
            user.setState(User.State.CONFIRMED);
            userRepository.save(user);

            return UserDto.from(user);
        } else return null;
    }

    @Override
    public List<UserDto> getLastArtists(int count) {
        List<User> artists = userRepository.findByIsArtist(true);
        if (artists.size() > count) artists = artists.subList(artists.size() - 1 - count, artists.size());

        return artists.stream()
                .map(UserDto::from)
                .collect(Collectors.toList());
    }

    @Override
    public User getAdmin() {
        return authDataRepository.findByRole(AuthData.Role.ADMIN).getUser();
    }
}
