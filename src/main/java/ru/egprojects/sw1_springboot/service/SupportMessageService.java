package ru.egprojects.sw1_springboot.service;

import ru.egprojects.sw1_springboot.dto.SupportMessageDto;
import ru.egprojects.sw1_springboot.model.User;

import java.util.List;

public interface SupportMessageService {
    void save(SupportMessageDto dto);

    List<SupportMessageDto> getDialog(String firstLogin, String secondLogin);

    List<SupportMessageDto> getAllMessages(User admin);

}
