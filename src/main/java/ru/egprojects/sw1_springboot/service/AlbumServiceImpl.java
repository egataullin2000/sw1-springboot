package ru.egprojects.sw1_springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.egprojects.sw1_springboot.dto.AlbumDto;
import ru.egprojects.sw1_springboot.model.Album;
import ru.egprojects.sw1_springboot.model.User;
import ru.egprojects.sw1_springboot.repository.AlbumRepository;
import ru.egprojects.sw1_springboot.repository.UserRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class AlbumServiceImpl implements AlbumService {

    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Album createAlbum(
            User artist,
            String title,
            String genre,
            int year,
            String description,
            MultipartFile poster,
            MultipartFile background,
            MultipartFile[] audios
    ) {
        Album album = Album.builder()
                .artist(artist)
                .title(title)
                .genre(genre)
                .year(year)
                .description(description)
                .build();
        albumRepository.save(album);

        album.setPoster(fileStorageService.saveFile(poster));
        album.setBackground(fileStorageService.saveFile(background));

        album.setAudios(
                Arrays.stream(audios)
                        .map((file) -> fileStorageService.saveFile(file))
                        .collect(Collectors.toList())
        );

        updateAlbum(album);

        List<Album> albums = artist.getAlbums();
        if (albums == null) albums = new ArrayList<>();
        albums.add(album);
        artist.setAlbums(albums);
        userRepository.save(artist);

        return album;
    }

    @Override
    public AlbumDto getAlbumById(Long id) {
        return AlbumDto.from(albumRepository.getOne(id));
    }

    @Override
    public void updateAlbum(Album album) {
        albumRepository.save(album);
    }

    @Override
    public List<AlbumDto> search(String query, Integer year) {
        Stream<AlbumDto> data = albumRepository.findAll()
                .stream()
                .filter(album -> album.getTitle().contains(query))
                .map(AlbumDto::from);
        if (year != null) data = data.filter(album -> album.getYear() == year);

        return data.collect(Collectors.toList());
    }

    @Override
    public List<AlbumDto> getLast(int count) {
        List<Album> data = albumRepository.findAll();
        if (data.size() > count) data = data.subList(data.size() - count - 1, data.size());

        return data.stream()
                .map(AlbumDto::from)
                .collect(Collectors.toList());
    }
}
