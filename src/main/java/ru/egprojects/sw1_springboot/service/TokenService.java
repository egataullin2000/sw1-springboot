package ru.egprojects.sw1_springboot.service;

import ru.egprojects.sw1_springboot.dto.SignInDto;
import ru.egprojects.sw1_springboot.dto.TokenDto;

public interface TokenService {
    TokenDto signIn(SignInDto signInDto);
}
