package ru.egprojects.sw1_springboot.service;

import org.springframework.web.multipart.MultipartFile;
import ru.egprojects.sw1_springboot.dto.AlbumDto;
import ru.egprojects.sw1_springboot.model.Album;
import ru.egprojects.sw1_springboot.model.User;

import java.util.List;

public interface AlbumService {
    Album createAlbum(
            User artist,
            String title,
            String genre,
            int year,
            String description,
            MultipartFile poster,
            MultipartFile background,
            MultipartFile[] audios
    );

    AlbumDto getAlbumById(Long id);

    void updateAlbum(Album album);

    List<AlbumDto> search(String query, Integer year);

    List<AlbumDto> getLast(int count);
}
