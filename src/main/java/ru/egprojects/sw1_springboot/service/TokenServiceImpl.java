package ru.egprojects.sw1_springboot.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.egprojects.sw1_springboot.dto.SignInDto;
import ru.egprojects.sw1_springboot.dto.TokenDto;
import ru.egprojects.sw1_springboot.model.AuthData;
import ru.egprojects.sw1_springboot.model.User;
import ru.egprojects.sw1_springboot.repository.AuthDataRepository;

import java.util.Optional;

@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private AuthDataRepository authDataRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Value("${jwt.secret}")
    private String secret;

    @Override
    public TokenDto signIn(SignInDto signInDto) {
        Optional<AuthData> authDataOptional = authDataRepository.findByEmail(signInDto.getEmail());
        if (authDataOptional.isPresent()) {
            AuthData authData = authDataOptional.get();
            User user = authData.getUser();
            if ((passwordEncoder.matches(signInDto.getPassword(), authData.getPasswordHash())) &&
                    (user.getState().equals(User.State.CONFIRMED))) {
                String token = Jwts.builder()
                        .setSubject(user.getId().toString())
                        .claim("name", user.getUsername())
                        .claim("role", authData.getRole().name())
                        .signWith(SignatureAlgorithm.HS256, secret)
                        .compact();
                return new TokenDto(token);
            } else throw new org.springframework.security.access.AccessDeniedException("Wrong email/password");
        } else throw new AccessDeniedException("User not found");
    }

}
