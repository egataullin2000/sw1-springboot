package ru.egprojects.sw1_springboot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.egprojects.sw1_springboot.dto.SupportMessageDto;
import ru.egprojects.sw1_springboot.model.SupportMessage;
import ru.egprojects.sw1_springboot.model.User;
import ru.egprojects.sw1_springboot.repository.SupportMessagesRepository;
import ru.egprojects.sw1_springboot.repository.UserRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SupportMessageServiceImpl implements SupportMessageService {

    @Autowired
    private SupportMessagesRepository messagesRepository;

    @Autowired
    private UserRepository usersRepository;

    @Override
    public void save(SupportMessageDto dto) {
        SupportMessage message;

        Optional<User> sender = usersRepository.findByEmail(dto.getSender());
        Optional<User> receiver = usersRepository.findByEmail(dto.getReceiver());
        if (sender.isPresent() && receiver.isPresent()) {
            message = SupportMessage.builder()
                    .sender(sender.get())
                    .text(dto.getText())
                    .receiver(receiver.get())
                    .build();


            messagesRepository.save(message);
        }
    }

    @Override
    public List<SupportMessageDto> getDialog(String firstLogin, String secondLogin) {
        List<SupportMessage> dialogRaw = messagesRepository.getAllBySenderEmailAndReceiverEmail(firstLogin, secondLogin);
        dialogRaw.addAll(messagesRepository.getAllBySenderEmailAndReceiverEmail(secondLogin, firstLogin));
        dialogRaw.sort(Comparator.comparing(SupportMessage::getId));
        return dialogRaw.stream().map(msg -> SupportMessageDto.builder()
                .text(msg.getText())
                .receiver(msg.getReceiver().getEmail())
                .sender(msg.getSender().getEmail())
                .build())
                .collect(Collectors.toList());

    }

    @Override
    public List<SupportMessageDto> getAllMessages(User admin) {
        List<SupportMessage> supportMessages = messagesRepository.getAllMessages(admin);
        return supportMessages.stream().map(msg -> SupportMessageDto.builder()
                .text(msg.getText())
                .receiver(msg.getReceiver().getEmail())
                .sender(msg.getSender().getEmail())
                .build())
                .collect(Collectors.toList());
    }
}